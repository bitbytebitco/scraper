#!/home/beckerz/envs/scraper/bin/python
import os
import requests
from bs4 import BeautifulSoup
import argparse



def fetch_url(url):
    r = requests.get(url)
    return BeautifulSoup(r.text, 'html.parser')

def get_links(soup):
    return soup.find_all("a")

def get_local_links(soup):
    l = get_links(soup)
    ll = [l.attrs['href'] for l in links if 'https' not in l.attrs['href'] and 'index' not in l.attrs['href']]
    return ll

def get_images_srcs(soup):
    imgs = soup.find_all("img")
    return [img['src'] for img in imgs]    

def save_file(soup, path):
    print("")
    print("** save_file **")
    try:
        if '/' in path:
            print(' has slash / ')
            print(path)
            print(os.path.split(path))
            create_dir(os.path.split(path)[0])
        with open(path, 'w+', encoding='utf-8') as f_out:
            f_out.write(soup.prettify())
    except Exception as e:
        print("save_file issue")
        print(e)

def save_image(url, path):
    try: 
        r = requests.get(url)
        if r.status_code == 200:
            with open(path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
    except Exception as e:
        print(e)

def download_page(page_url):
    s = fetch_url(page_url)
    iurls = get_images_srcs(s) 
    #print(iurls)
    dest_path = ll.replace('.html','')
    print(dest_path)

    save_file(s, ll)
    download_page_images(iurls, dest_path)
    download_head_files(s.head)

def download_head_files(soup):
    print("LINKS")
    for l in soup.find_all(["link"]):
        if 'http' not in l.attrs['href']:
            page_url = "%s%s" % (args.url,l.attrs['href'])
            s = fetch_url(page_url)
            save_file(s, l.attrs['href'])
    print("") 
    print("SCRIPTS")
    for l in soup.find_all(["script"]):
        if 'http' not in l.attrs['src']:
            page_url = "%s%s" % (args.url,l.attrs['src'])
            s = fetch_url(page_url)
            save_file(s, l.attrs['src'])

def create_dir(path):
    print('**')
    print('create_dir')
    try:
        print(path)
        if not(os.path.exists(path)):
            print("%s doesnt exist" % path)
            os.makedirs(path)
            print("%s created" % path)
    except Exception as e:
        print('effed up')
        print(e)
    print("create_dir end")

def download_page_images(urls, dest_path):
    try:
        
        for url in urls:
            u = "%s%s" % (args.url,url)
            path = u.split('/')[-1]
            dest_folder = u.split('/')[-2]
            #print(dest_folder)
            save_path = os.path.join(dest_folder, path)
            #print(save_path)
            create_dir(dest_folder)
            save_image(u, save_path)
    except Exception as e:
        print(e)

def get_url_contents(url):
    r = requests.get(url)
    return fetch_url(url)

def get_cl_args():
    parser = argparse.ArgumentParser(description='Web scraping tool')
    parser.add_argument('url', help="url to scrape")

    return parser.parse_args()

def main():
    try:
        args = get_cl_args()
        s = get_url_contents(args.url)
        local_links = get_local_links(s) 
        for ll in local_links:
            page_url = "%s%s" % (args.url,ll)
            print(page_url)
            download_page(page_url)
         
    except Exception as e:
        print(e)

if __name__ == "__main__":
    main()
