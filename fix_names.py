import sys
sys.path.append('/var/www/video_encyclopedia')
from bs4 import BeautifulSoup
import requests
import dbhandler

db = dbhandler.DB()

def find_broken_links():
    articles = db.select_from('published_articles') 
    count = 0
    all_links = []
    for a in articles:
        count = count + 1
        soup = BeautifulSoup(a['content'], 'html.parser') 
        links = soup.find_all('a')
        for l in links:
            href = l.attrs['href']
            href = href.replace("https://videncyc.diluo.org","https://bjjpath.com")
            if 'http' in href and href not in all_links:
                all_links.append(href) 
   
    bad_links = []
    if all_links is not None:
        for href in all_links:
            try: 
                status_code = get_status_code(href)
                #print(status_code)
                if status_code is not 200:
                    bad_links.append((status_code, href))         
            except Exception as e:
                print(e)
    return bad_links

def get_slug(link):
    return link.split('/')[-1]

def fix_broken_links(broken_links):
    #print(broken_links)
    print(len(broken_links))
    for bl in broken_links:
        print(bl[1])
        correct_slug = get_slug(bl[1])
        #print(correct_slug)
    
        title = get_title_from_hubpages("https://howtheyplay.com/individual-sports/{}".format(correct_slug))
        #print("title_from_hp: {}".format(title))
        article = get_article_by_title(title)
        if article is not None:
            print(article)
            update_article_slug(correct_slug, article['id'])
            print("new slug: {}".format(correct_slug))

def update_article_slug(slug, article_id):
    try:
        db.query("update_article_slug", {"slug":slug, "article_id":article_id}) 
    except Exception as e:
        print(e)

def get_article_by_title(title):
    try: 
        if title is not None:
            print(title)
            db.cursor.execute("select id,title, slug from articles where title = '{}' and published=1".format(title))
            rows = db.cursor.fetchall() 
            if len(rows)>0:
                return rows[0]
            return None
    except Exception as e:
        print('FUNCTION: get_article_by_title')
        print(e)

def get_title_from_hubpages(url):
    try:
        r = requests.get(url)
        soup = BeautifulSoup(r.text, 'html.parser') 
        title = soup.find_all(class_="moduleTitle")
        if len(title)>0:
            return str(title[0].h1.string)
    except Exception as e:
        print(e)
    return None
    
def get_status_code(href):
    s = requests.Session()
    user_v = "2|1:0|10:1548311883|4:user|176:eyJ1c2VyX2lkIjogMSwgInJvbGVzIjogWyJzdXBlcnVzZXIiLCAic3Vic2NyaWJlciJdLCAiZW1haWwiOiAiYml0Ynl0ZWJpdGNvQGdtYWlsLmNvbSIsICJzdHJpcGVfY3VzdG9tZXJfaWQiOiBudWxsLCAiY29tcGVkIjogZmFsc2V9|87549d4cb406e8c2abfb4119932980b18ba80788803b22da1eaabc6e25ae7504"
    try:
        r = requests.get(href, cookies={'user':user_v})
        return r.status_code
        #print(r.text)
    except Exception as e:
        print(e)      
         
    
def get_double_dash_slugs():
    db.cursor.execute("select id,title, slug from articles where slug like '%--%'")
    rows = db.cursor.fetchall() 
    for row in rows:
        print(row)

if __name__ == "__main__":
    #get_double_dash_slugs()
    broken_links = find_broken_links()
    fix_broken_links(broken_links)

    #test_link("https://bjjpath.com/article/How-to-Take-the-Back-from-Turtle2")
    #title = get_title_from_hubpages("https://howtheyplay.com/individual-sports/Leg-Drag-to-Back-Take-a-BJJ-Tutorial")
    #article = get_article_by_title(title)
    #print(article)
    #update_article_slug("Leg-Drag-to-Back-Take-a-BJJ-Tutorial", article['id'])
