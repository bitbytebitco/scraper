# Description
This scraper tool was originally built to download a legacy version of a client's website.
This core tool was then implemented (in tut_scraper.py) to scrape and insert another client's tutorials from the content's previous location into the new platform provided to them.

## Command Line Tool 
```bash
usage: scraper.py [-h] url

Web scraping tool

positional arguments:
  url         url to scrape

optional arguments:
  -h, --help  show this help message and exit
```

## Dependencies
For python environment:
```
beautifulsoup4==4.6.3
certifi==2018.11.29
chardet==3.0.4
idna==2.8
mysql-connector==2.1.6
requests==2.21.0
selenium==3.141.0
urllib3==1.24.1
```

## Additional Dependency note
[phantomjs-2.1.1](https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2) was also needed for tut_scraper.py to function properly on my installation of Ubuntu 16.04.6 LTS
