#!/home/beckerz/envs/scraper/bin/python
import sys
from copy import deepcopy
sys.path.append('/var/www/video_encyclopedia')
import dbhandler
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from bs4.element import Tag 
import scraper as s
import re
import urllib

processed = []

def getModuleText(el):
    contents = [c for c in el.find(class_="txtd").contents]
    return contents
    #return " ".join(contents)

def get_page_data(url, driver=None):
    # get contents of class 'full module moduleTitle'
    if driver is None:
        print("BeautifulSoup ROUTE")
        data = s.get_url_contents(url)
    else:
        print("webdriver.PhantomJS ROUTE")
        driver.get(url)
        data = BeautifulSoup(driver.page_source, 'html.parser')     
    return data

def getModuleh2(el):
    sub = el.find(class_="subtitle")
    if sub is not None:
        c = sub.contents
        if c is not None and len(c)>0:
            return str(c[0])

def build_subtitle_tag(soup, data):
    h3 = soup.new_tag('h3')
    h3.attrs['class'] = 'mt-4 mb-2'
    h3.string = data
    return h3
    #return "<h3 class='mt-4 mb-2'>{}</h3>".format(data)

def new_img_tag(soup, iframe, youtube_id):
    img = soup.new_tag('img')
    img.attrs['alt'] = 'Video'
    img.attrs['ref'] = "youtube_id-{0}".format(youtube_id)
    img.attrs['src'] = "https://img.youtube.com/vi/{0}/default.jpg".format(youtube_id)
    return img 

def build_img_tag(youtube_id): # deprecated
    return '<img alt="Video" src="https://img.youtube.com/vi/{0}/default.jpg" ref="youtube_id-{0}">'.format(youtube_id)

def get_tutorial_contents(data):
    contents = []
    article_links = None
    clean_links = None

    elements = data.find_all(class_=re.compile("(full module moduleText|full module moduleVideo)"))
    for el in elements:
        if 'moduleText' in el.attrs['class']:
            #c = el.find(class_="txtd").contents
            subtitle = getModuleh2(el)
            if subtitle is not None:
                h3 = build_subtitle_tag(data, subtitle)
                contents.append(h3)
            text_list = getModuleText(el)
            for c in text_list: 
                contents.append(c)
        if 'moduleVideo' in el.attrs['class']:
            iframe = el.find("iframe")
            src = iframe['src']
            youtube_id = src.split('/')[4].split('?')[0]
            img = new_img_tag(data, iframe, youtube_id)
            contents.append(img)
    article_links = get_tutorial_hrefs(contents)
    cleaned_links, contents = replace_article_links(article_links, contents)
    
    #print("CLEANED LINKS")
    #print(cleaned_links)
    
    article_html = gen_article_html_str(contents)

    return article_html, cleaned_links 

def build_og_links(links):
    # DEPRECATED
    #TODO: Fix this so that is returns cleaned urls (list of strings)
    ret = []
    for l in links:
        ret.append(str(l))
    return ret

def gen_article_html_str(contents):
    article_html = ""
    for tag in contents: 
        article_html = article_html + "\n{}".format(str(tag))
    return article_html

def get_blacklist():
    return ['javascript','youtube', 'revolutionbjj']

def get_tutorial_hrefs(data):
    #links = s.get_links(data)
    whitelist = ['howtheyplay'] 
    blacklist = get_blacklist()
    keepers = set() 
    for el in data:
        if isinstance(el, Tag): 
            links = el.find_all("a")
            for l in links: 
                if 'href' in l.attrs:
                    bad = False
                    for b in blacklist:
                        if b in l.attrs['href']:
                            print("")
                            print("!!!!")
                            print("FOUND BAD THING: {}".format(b))
                            bad = True     
                    if not bad:
                        for w in whitelist:
                            if w in l.attrs['href']:
                                keepers.add(l)
    print("")
    print("KEEPERS")
    print(keepers)
    return list(keepers)

def get_title(data):
    title = data.find_all(class_="moduleTitle")
    if len(title)>0:
        return str(title[0].h1.string)

def gen_tags(data):
    # TODO: make this work
    words = set()
    for e in data:
        words.add(str(e.string))
    return words 

def check_article_exists(slug, db):
    res = db.select_from("get_article_by_slug", opts={'slug':slug}) 
    if res is not None:
        return True 
    else:
        return False

def process_article_links(links, db, driver, single=False):
    print('')
    print('')
    print('***')
    print('ON DECK:')
    print(links)

    global processed
    count = 0
    on_deck = []

    for link in links:
        og_link = link
        if not isinstance(link, str):
            link = link.attrs['href'] 
        slug = link.split('/')[-1]
        title = link.split('/')[-1].replace('-',' ').strip()
        print("")
        print("")
        print('checking slug: {}'.format(slug))
        print('URL : {}'.format(link))
        print("")
        existing = check_article_exists(slug, db)
        if existing:
            print('slug exists: {}'.format(slug))
            continue
        else:
            result, article_links = process_article(link, slug, db, driver) 
            if result:
                links.remove(og_link)
                print('processed: {}'.format(title))
                processed.append(title)
                if len(article_links)>0:
                    on_deck = list(set(links + article_links))
                else:
                    on_deck = links
     
    if len(on_deck)>0 and single is False:
        process_article_links(on_deck, db, driver)
    else: 
        print("ON DECK IS EMPTY")

def replace_article_links(links, data):
    print('')
    print('replace_article_links')
    domain = "videncyc.diluo.org/article"
    howtheyplay = "howtheyplay.com/individual-sports"
    cleaned_links = []
    for tag in data:
        if isinstance(tag, Tag): 
            for link in tag.find_all('a'):
                if 'href' in link.attrs:
                    print('link found')
                    print(link)
                    bad = False
                    for b in get_blacklist():
                        if b in link.attrs['href']:
                            bad = True 
                    if not bad:
                        if 'redir' in link.attrs['href']: 
                            encoded_url = link.attrs['href'].rsplit('redirectUrl=')[1].split('&')[0]
                            unquoted = urllib.parse.unquote(encoded_url)
                            print(unquoted)
                            title = unquoted.split('/')[-1]
                        else:
                            title = link.attrs['href'].split('/')[-1]
                        link.attrs['href'] = 'https://{}/{}'.format(domain, title)
                        cleaned_links.append('https://{}/{}'.format(howtheyplay, title))
                else:
                    print("")
                    print("href missing?")
                    print(link)
        else:
            pass
            #print("")
            #print("FOUND something other than a tag.")
            #print(tag)
    return cleaned_links, data

def process_article(url, slug, db, driver):
    print("PROCESS ARTICLE")
    print(url)
    data = get_page_data(url, driver=driver)
    title = get_title(data)
    article_html, cleaned_links = get_tutorial_contents(data)
    article_html = article_html.replace("<p><br /></p>", "")
    #article_html = "\n".join(contents)
    
    #tut_bs = BeautifulSoup(article_html, 'html.parser')
    #tags = gen_tags(tut_bs)
    #print(tags)
    print("SLUG: {}".format(slug))
    if title is not None and slug is not None and article_html is not None: 
        article = {
            "title":title,
            "slug": slug,
            "category_id":1,
            "user_id":"1",
            "content": article_html,
            "group_id":0,
            "blurb": "",
            "featured_flag": 0,
        }
        row_id = db.insert_article(article)
        if row_id:
            return True, cleaned_links 
    else:
        print("title or article_html is empty")
        return False

def main():

    db = dbhandler.DB()

    args = s.get_cl_args()

    #tut_url = "https://howtheyplay.com/individual-sports/how-to-pass-the-guard-using-the-kimura"
   
    driver = webdriver.PhantomJS()
    
    process_article_links([args.url], db, driver, single=True) 
        
    
if __name__ == "__main__":
    main()
